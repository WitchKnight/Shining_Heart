use ::opengl_graphics::{ OpenGL };
use ::piston_window::*;
use ::gfx_device_gl;
use ::gfx_device_gl::Factory;
use ::graphics;
use ::soundsystem;
use ::soundsystem::*;

#[path = "game.rs"]
mod game;
use self::game::*;



//~ #[path = "sound.rs"]
//~ mod sound;
//~ use self::sound::*;


use ::external::*;


pub struct App {
    ext_settings  : ESettings,
    gamestate     : GameState
}

impl App {
  pub fn new(es: ESettings) -> App {
    App {
      ext_settings  : es,
      gamestate     : GameState::new(es),
    }
  }
  pub fn new_window(&self) -> PistonWindow {
    let opengl = OpenGL::V3_2;
    WindowSettings::new("Shining heart", self.ext_settings.dims)
                        .exit_on_esc(true)
                        .opengl(opengl)
                        .build()
                        .unwrap()
  }
  pub fn run(&mut self) {
    let mut win = self.new_window();
    while let Some(e) = win.next() {
    //   self.gamestate.debug();
      let mut f = win.factory.clone();
      if let Some(r) = e.render_args() {
        self.initialize_rendering(r,f.clone());
        
        win.draw_2d(&e, |c,g| {
          self.display_game(r,f,c,g)
        });
      } 
      if let Some(u) = e.update_args() {
        self.update_app(u);
      }
      if let Some(m) = e.mouse_cursor_args() {

      }
      if let Some(inp) = e.press_args() {
        self.gamestate.on_press(inp);
      }
      if let Some(inp) = e.release_args() {

      }

    }
  }
  fn display_game(&mut self, r: RenderArgs, mut f: Factory, c: graphics::Context, g: &mut G2d ) {
      self.gamestate.render_game(c,g,r);
  }
  fn update_app(&mut self, u: UpdateArgs) {
    self.gamestate.update(u);
  }
  fn initialize_rendering(&mut self, r: RenderArgs, f: Factory) {
    if self.gamestate.loaded == false {
      self.gamestate.initialize_rendering(r,f.clone());
    }
    self.gamestate.loaded = true;
  }
  
}
