#![feature(slice_patterns)]
#[macro_use]
extern crate json;
extern crate rodio;
extern crate opengl_graphics;
extern crate piston_window;
extern crate find_folder;
extern crate gfx_device_gl;
extern crate sprite;
extern crate graphics;
extern crate ai_behavior;
#[macro_use]
extern crate enum_map;
#[macro_use]
extern crate enum_map_derive;

mod app;
mod external;

use external::*;

mod soundsystem;
use soundsystem::*;


fn main() {
  let es = load_external();
  let mut app = app::App::new(es);
  app.run()
}
