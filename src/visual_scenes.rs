  
  
  
  
  
  
  
  
  
  
  pub fn get_scene(&self, r: RenderArgs,mut f: Factory) -> Scene<ImageType> {
    match self {
      &GameStage::Launch    => {Scene::new()}
      &GameStage::Loading   => {Self::display_loading(r,f)}
      &GameStage::Menu(_)      => {Scene::new()}
      &GameStage::InGame(_)    => {Scene::new()}
    }
  }
  fn display_loading(r: RenderArgs,mut f:Factory) -> Scene<ImageType> {
    let (wf,hf) = (r.width as f64, r.height as f64);
    let uielems = PathBuf::from("../assets/ui/loading");
    let tex1 = Rc::new(Texture::from_path(
              &mut f,
              uielems.join("maculed heart.png"),
              Flip::None,
              &TextureSettings::new()
          ).unwrap());
    let tex2 = Rc::new(Texture::from_path(
              &mut f,
              uielems.join("larger heart.png"),
              Flip::None,
              &TextureSettings::new()
          ).unwrap());
    let mut sc = Scene::new();
    let mut hsprite = Sprite::from_texture(tex1.clone());
    let mut lhsprite = Sprite::from_texture(tex2.clone());
    
    hsprite.set_position(2.0*wf/3.0,hf/2.0);
    lhsprite.set_position(2.0*wf/3.0,hf/2.0);
    lhsprite.set_scale(1.2,1.2);
    lhsprite.set_opacity(0.6);
    let id1 = sc.add_child(hsprite);
    let id2 = sc.add_child(lhsprite);

    sc
    
  }
