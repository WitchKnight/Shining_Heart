use ::json;
use ::find_folder::*;

use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::io::BufReader;
use std::error::Error;


#[derive(PartialEq, Eq, Debug, Clone,Copy)] 
pub struct ESettings {
  pub dims: (u32,u32),
  pub vol:  [u32;2]
  
}

impl ESettings {
  pub fn new() -> ESettings {
    ESettings {
      dims  : (0,0),
      vol   : [1,1]
    }
  }
}


pub fn load_external() -> ESettings {
  let settings = get_config();
  settings
}

fn get_config() -> ESettings {
  let path = Search::ParentsThenKids(3, 3).for_folder("config").unwrap();
  let file = match File::open(path.join("config.json")) {
    Err(why) => { panic!("couldn't open the configuration file : {}", why.description())}
    Ok(file) => file
  };
  let mut buf_reader = BufReader::new(file);
  let mut contents = String::new();
  buf_reader.read_to_string(&mut contents).unwrap();
  let parsed = json::parse(&contents).unwrap();
  let mut eset = ESettings::new();
  eset.dims = (parsed["dimensions"][0].as_u32().unwrap()  ,parsed["dimensions"][1].as_u32().unwrap() );
  eset.vol = [parsed["volume"][0].as_u32().unwrap()  ,parsed["volume"][1].as_u32().unwrap()];
  eset
}

pub fn save_settings(es: ESettings) {
  let (w,h) = es.dims;
  let [vm,vs] = es.vol;
  let esjson = object!{
    "dimensions" => array![w,h],
    "volume" => array![vm,vs]
  };
  let mut file = match File::create("../config/config.json") {
    Err(why) => {panic!("couldn't create conf file: {}", why.description())}
    Ok(file) => {file}
  };
  match file.write_all(esjson.dump().as_bytes()) {
        Err(why) => {
            panic!("couldn't write: {}", why.description())
        },
        Ok(_) => println!("successfully wrote"),
    }
}

