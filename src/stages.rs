

use ::enum_map_derive;
use ::gfx_device_gl;
use ::gfx_device_gl::Factory;
use ::ai_behavior::{
    Action,
    Sequence,
    Wait,
    WaitForever,
    While,
};

use ::piston_window::*;
use ::piston_window;
use ::sprite::*;


use std::collections::*;
use std::rc::Rc;
use std::path::*;
use std::fmt::{self, Debug, Display};

#[path = "convenience.rs"]
#[macro_use]
mod convenience;

use self::GameStage::*;
use self::Page::*;
use self::SubPage::*;
use self::SubOption::*;
use self::Phase::*;
use self::BattlePhase::*;




#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum GameStage { 
  Menu(Page), 
  InGame(Phase), 
  Loading, 
  Launch
}
#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum Page {
  Main(Option<SubPage>),
  Quit
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum SubPage {
  Options(Option<SubOption>),
  NewGame,
  Load,    
  Quit
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum SubOption { Sound, Gameplay, Graphics }

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum Phase { 
  Battle(BattlePhase),
  Cinematic(Option<CMenu>),
  Explore(Option<GMenu>)
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum CMenu {
  Resume,
  Skip,
  Quit
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum GMenu {
 Resume
}



#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum BattlePhase { Prepare, Turn, Execute, Combat, Event, Check, End }




//Yeee boyy


impl fmt::Display for GameStage { // this is just here to allow me to call  .to_string()
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}

impl fmt::Display for Page { // this is just here to allow me to call  .to_string()
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}
impl fmt::Display for SubPage { // this is just here to allow me to call  .to_string()
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}
impl fmt::Display for SubOption { // this is just here to allow me to call  .to_string()
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}
impl fmt::Display for Phase { // this is just here to allow me to call  .to_string()
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}
impl fmt::Display for CMenu { // this is just here to allow me to call  .to_string()
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}
impl fmt::Display for GMenu { // this is just here to allow me to call  .to_string()
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}


pub trait GameDiv {fn convert (&self) -> u32 ;}

impl GameDiv for GameStage {
  fn convert (&self) -> u32  {
    match self {
      &GameStage::Launch     => 0 ,
      &GameStage::Loading    => 1 ,
      &GameStage::Menu(_)    => 2 ,
      &GameStage::InGame(_)  => 3 ,
    }
  }
}

impl GameDiv for Page {
  fn convert (&self) -> u32  {  
    match self {
      &Page::Main(_)         => 4 ,
      &Page::Quit            => 5 ,
    }
  }
}

impl GameDiv for SubPage {
  fn convert (&self) -> u32  {
    match self {
      &SubPage::Options(_)  => 6 ,
      &SubPage::Load        => 7 ,
      &SubPage::NewGame     => 26 ,
      &SubPage::Quit        => 8 ,

    }
  }
}

impl GameDiv for SubOption {
  fn convert (&self) -> u32  {
    match self {
      &SubOption::Gameplay   => 9 ,
      &SubOption::Graphics   => 10,
      &SubOption::Sound      => 11,
    }
  }
}


impl GameDiv for Phase {
  fn convert (&self) -> u32  {
    match self {
      &Phase::Battle(_)      => 12,
      &Phase::Cinematic(_)   => 13,
      &Phase::Explore(_)     => 14,
    }
  }                               
}
impl GameDiv for CMenu {
  fn convert (&self) -> u32  {
    match self {
      &CMenu::Resume         => 15,
      &CMenu::Skip           => 16,
      &CMenu::Quit           => 17,
    }
  }
}
impl GameDiv for GMenu {
  fn convert (&self) -> u32  {
    match self {
      &GMenu::Resume         => 18
    }
  }
}

impl GameDiv for BattlePhase {
  fn convert (&self) -> u32  {
    match self {
      &BattlePhase::Prepare         => 19,
      &BattlePhase::Turn            => 20,
      &BattlePhase::Execute         => 21,
      &BattlePhase::Combat          => 22,
      &BattlePhase::Event           => 23,
      &BattlePhase::Check           => 24,
      &BattlePhase::End             => 25
    }
  }
}


impl GameStage {
  pub fn deconstruct(&self) -> Vec<u32> {
    let &t = self; 
    let mut v = Vec::with_capacity(32);
    v.push(t.convert());
    match t {
      Menu(t2) => {
        v. push(t2.convert());
        match t2 {
          Main(o1)  => {
            match o1 {
              Some(t3)  => {
                v.push(t3.convert());
                match t3 {
                  Options(o2) => {
                    match o2 {
                      Some(t4) => {v.push(t4.convert())}
                      None     => {}
                    }
                  }
                  _           => {}
                }
              }
              None      => {}
            }
          }
          _              => {}
        }
      }
      InGame(t2) => {
        v. push(t2.convert());
        match t2 {
          Battle(t3)    => { v.push(t3.convert()) }
          Cinematic(o1) => {
            match o1  {
              Some(t3)    => { v.push(t3.convert())}
              None        => {}
            }
          }
          Explore(o2)   => {
            match o2  {
              Some(t3)    => { v.push(t3.convert())}
              None        => {}
            }
          }
        }
      }
      _                 => {}
    }
    v
  }
}
