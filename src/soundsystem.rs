use ::rodio;
use ::rodio::*;
use ::rodio::source::*;
use ::rodio::decoder::*;
use ::find_folder::*;


use self::MusicT::*;
use self::SoundEffectT::*;

use std;
use std::path::Path;
use std::rc::Rc;
use std::io::BufReader;
use std::fmt::{self, Debug, Display};
use std::fs::File;
use std::collections::*;
use std::collections::hash_map::Entry::*;



#[derive(PartialEq, PartialOrd, Ord, Eq, Hash, Debug, Clone, Copy)] 
pub enum SoundEffectT { Confirm, Cancel, Step,
                        Start, Victory,
                        CutAttack, PierceAttack, CotundAttack
                         }
                         
impl fmt::Display for SoundEffectT {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}


#[derive(PartialEq, PartialOrd, Ord, Eq, Hash, Debug, Clone, Copy)]                       
pub enum MusicT { Grol, Fall, End, Opening, Silence, Pulse }

impl fmt::Display for MusicT {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}


type ThemeFromFile = Buffered<Decoder<BufReader<File>>>;
type SoundFromFile = Buffered<Decoder<BufReader<File>>>;


pub struct SoundSystem {
    themes    : HashMap<MusicT,ThemeFromFile>
  , sounds    : HashMap<SoundEffectT,SoundFromFile>
  , loaded    : bool
  , volume    : [f32;2]
  , endpoint  : Option<Endpoint>
  , sink      : Option<Sink>
}



impl SoundSystem {
  pub fn new() -> SoundSystem {
    let mut baseThemes = HashMap::with_capacity(10);
    let mut baseSounds    = HashMap::with_capacity(20);
    SoundSystem {
      themes    : baseThemes,
      sounds    : baseSounds,
      loaded    : false,
      volume    : [0.0,0.0],
      endpoint  : None,
      sink      : None 
    }
  }
  pub fn set_volume(&mut self,v: [f32;2]) {
    self.volume = v
  }
  pub fn load(&mut self) {
    self.load_theme(Grol);
    self.load_theme(Fall);  
    self.load_theme(End);   
    self.load_theme(Pulse);   
    self.load_theme(Opening);
    self.load_sound(Confirm);
    self.load_sound(Cancel);
    self.load_sound(Step);
    self.endpoint = Some(rodio::get_default_endpoint().unwrap());
    self.loaded = true;
  }
  pub fn load_theme(&mut self, t: MusicT) {
    let themes = Search::ParentsThenKids(3, 3).for_folder("music").unwrap();
    let theme = t.to_string()+".ogg";
    print! ("{}",theme);
    self.themes.insert(t, (Decoder::new(BufReader::new(File::open(themes.join(theme)).unwrap()))).unwrap().buffered());
  }
  pub fn load_sound(&mut self, t: SoundEffectT) {
    let sounds = Search::ParentsThenKids(3, 3).for_folder("sound").unwrap();
    let sound = t.to_string()+".ogg";
    self.sounds.insert(t, (Decoder::new(BufReader::new(File::open(sounds.join(sound)).unwrap()))).unwrap().buffered());
  }
  pub fn get_theme(&self, t:MusicT) -> Option<ThemeFromFile> {
    let mut operathemes = self.themes.clone();
    let x = operathemes.entry(t);
    match x {
      Vacant(_)   =>  { None }
      Occupied(mut o) =>  { Some(o.remove())}
    }
  }
  pub fn get_sound(&self, t:SoundEffectT) -> Option<SoundFromFile> {
    let mut operasounds = self.sounds.clone();
    let x = operasounds.entry(t);
    match x {
      Vacant(_)   =>  { None }
      Occupied(mut o) =>  { Some(o.remove())}
    }
  }
  pub fn play_sound (&self, t: SoundEffectT) {
    match self.endpoint {
      None => {}
      Some(ref e) => {
        let sound_sink = rodio::Sink::new(e);
        let volume = self.volume;
        let sound = self.get_sound(t).unwrap().amplify(volume[1]);
        sound_sink.append(sound);
        sound_sink.detach()
      }
    }
  }
  pub fn play_music (&mut self,t:MusicT){
    match self.endpoint {
        None => {}
        Some(ref ep) => {
          let volume = self.volume;
          let theme = self.get_theme(t).unwrap().amplify(volume[0]);
          match  &self.sink {
            &None => {
              let sinko = rodio::Sink::new(ep);
              sinko.append(theme);
              self.sink = Some(sinko);
            }
            _ => {}
          }
        }
    }
  }
  pub fn stop(&mut self) {
    self.sink = None
  }
  pub fn full_stop(&mut self) {
    self.sink = None;
    self.endpoint = None;
  }
}
