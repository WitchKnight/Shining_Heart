pub struct GameInput {
  pub confirm : bool,
  pub cancel  : bool,
  pub ctrl    : bool,
  pub alt     : bool,
  pub shift   : bool,
  pub up      : bool,
  pub down    : bool,
  pub left    : bool,
  pub right   : bool,
  pub click   : bool,
  pub r_click : bool,
  pub cursor  : (f64,f64)
}

impl GameInput {
  pub fn new() -> GameInput {
    GameInput {
      confirm : false,
      cancel  : false,
      ctrl    : false,
      alt     : false,
      shift   : false,
      up      : false,
      down    : false,
      left    : false,
      right   : false,
      click   : false,
      r_click : false,
      cursor  : (0.0,0.0)
    }
  }
  pub fn clear(&mut self) {
    self.confirm  = false;
    self.cancel   = false;
    self.ctrl     = false;
    self.alt      = false;
    self.shift    = false;
    self.up       = false;
    self.down     = false;
    self.left     = false;
    self.right    = false;
    self.click    = false;
    self.r_click  = false;
  }
}