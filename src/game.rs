use ::gfx_device_gl;
use ::gfx_device_gl::Factory;
use ::piston_window::*;
use ::graphics;
use ::sprite::*;
use ::soundsystem;
use ::soundsystem::*;
use ::external::ESettings;


#[path = "stages.rs"]
mod stages;
use self::stages::*;
use self::stages::GameStage::*;

#[path = "convenience.rs"]
#[macro_use]
mod convenience;
use self::convenience::*;


#[path = "render.rs"]
mod render;
use self::render::*;


#[path = "menu.rs"]
mod menu;

#[path = "input.rs"]
mod input;
use self::input::GameInput;

pub struct GameState {
  pub current_stage : GameStage               ,
  pub next_stage    : GameStage               ,
  turnover          : f64                     ,
  debug_flags       : [bool; 4]               ,
  rendering         : GameRender              ,
  pub loaded        : bool                    ,
  ext_settings      : ESettings               ,
  sound_system      : SoundSystem             ,
  input             : GameInput
}

impl GameState {
  pub fn new(es: ESettings) -> GameState {
    GameState { 
      ext_settings  : es,
      current_stage : Launch,
      next_stage    : Loading, 
      turnover      : 0.0,
      rendering     : GameRender::new(),
      debug_flags   : [false,true,false,false],
      loaded        : false,
      sound_system  : SoundSystem::new(),
      input         : GameInput::new()
    }
  }
  pub fn debug(&mut self) { 
    if self.debug_flags[0] {
      print!("{}", self.current_stage.to_string())
    }
  }
  pub fn render_game(&mut self,c: graphics::Context, g: &mut G2d, r: RenderArgs) {
    let d = self.current_stage.deconstruct();
    self.rendering.render(c,g,r,d);
  }
  pub fn update(&mut self, u: UpdateArgs) {
    self.update_time(u);
    match self.current_stage {
      Launch  => {
        self.trigger_next_stage(0.0);
        self.next_stage = Menu(Page::Main(None));
        self.first_load();
      }
      Loading => {
        self.trigger_next_stage(5.0);
      }
      _       => {}
    }
    self.process_input();
  }
  fn update_time(&mut self, u: UpdateArgs) {
    let d = self.current_stage.deconstruct();
    self.turnover += u.dt;
    self.rendering.update(d,u);
  }
  fn reset_time(&mut self,hard: bool) {
    let d = self.current_stage.deconstruct();
    self.turnover = 0.0;
    self.rendering.reset_time(d,hard);
  }
  fn trigger_next_stage(&mut self, time: f64) {
    if self.turnover > time {
      self.reset_time(true);
      self.current_stage = self.next_stage;
      self.trigger_music();
    }
  }
  fn trigger_music(&mut self) {
    match self.current_stage {
      Menu(_) => {self.sound_system.play_music(MusicT::Opening)}
      Loading => {self.sound_system.play_music(MusicT::Pulse)}
      _       => {}
    }
  }
  pub fn initialize_rendering(&mut self, r: RenderArgs, f: Factory) {
    self.rendering.build_visual_elements(r,f);
    self.loaded = true;
  }
  fn load_music(&mut self) {
    let es = self.ext_settings;
    self.sound_system.load();
    let vol = es.vol;
    self.sound_system.set_volume([vol[0] as f32 /100.0,vol[1] as f32 /100.0]);
  }
  fn first_load(&mut self) {
    self.load_music();
  }
  pub fn on_press(&mut self, inp: Button) {
     match inp {
        Button::Keyboard(Key::Up) => {
            self.input.up = true;
        }
        Button::Keyboard(Key::Down) => {
            self.input.down = true;
        }
        Button::Keyboard(Key::Left) => {
            self.input.left = true;
        }
        Button::Keyboard(Key::Right) => {
            self.input.right = true;
        }
        Button::Keyboard(Key::Space) => {
            self.input.confirm = true;
        }
        Button::Keyboard(Key::Return) => {
            self.input.confirm = true;
        }
        Button::Keyboard(Key::Escape) => {
            self.input.cancel = true;
        }
        Button::Keyboard(Key::LCtrl) => {
            self.input.ctrl = true;
        }
        Button::Mouse(MouseButton::Left) => {
            self.input.click = true;
        }
        Button::Mouse(MouseButton::Right) => {
            self.input.r_click = true;
        }
        _ => {}
     }
  }
  fn process_input(&mut self) {
    use soundsystem::SoundEffectT::*;
    if self.input.up == true {
      self.input.up = false;
      self.sound_system.play_sound(Step);
    }
    if self.input.down == true {
      self.input.down = false;
      self.sound_system.play_sound(Step);
    }
    if self.input.left == true {
      self.input.left = false;
      self.sound_system.play_sound(Step);
    }
    if self.input.right == true {
      self.input.right = false;
      self.sound_system.play_sound(Step);
    }
    if self.input.confirm == true {
      self.input.confirm = false;
      self.sound_system.play_sound(Confirm);
    }
    if self.input.cancel == true {
      self.input.cancel = false;
      self.sound_system.play_sound(Cancel);
    } 

  }
}
