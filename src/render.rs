use ::sprite::*;
use ::piston_window;
use ::piston_window::*;
use ::gfx_device_gl;
use ::gfx_device_gl::Factory;
use ::graphics;
use ::find_folder::*;
use ::graphics::math::Matrix2d;

use std::collections::*;
use std::rc::Rc;
use std::path::*;

use ::app::game::convenience::*;
use ::app::game::menu::*;


#[path = "stages.rs"]
mod stages;


use self::stages::*;
use self::stages::GameStage::*;


pub type ImageType = piston_window::Texture<gfx_device_gl::Resources>;  


pub struct GameText {
  style     : (String, u32,[f32; 4]), // Font, Size, Color 
  position  : (f64,f64),
  content   : String,
  glyphs    : Option<Glyphs>
}
  
impl GameText {
  pub fn new() -> GameText {
    GameText {
      style     : ("DUBIEL.TTF".to_string(),15,RED),
      position  : (0.0,1.0),
      content   : "DEFAULT".to_string(),
      glyphs    : None
    }
  }
  pub fn display(&mut self,c: graphics::Context, g: &mut G2d, r: RenderArgs) {
    let (wf,hf) = (r.width as f64, r.height as f64);
    let (x,y) = self.position;
    let (_,size,color) = self.style;
    match self.glyphs {
      Some(ref mut glyphs)  => { Text::new_color(color, size).draw(&self.content, glyphs, &c.draw_state, c.transform.trans(x*wf,y*hf), g) }  
      _             => {}
    }
  }
  pub fn from(s : String) -> GameText {
    let mut gt = GameText::new();
    gt.content = s;
    gt
  }
  pub fn build_glyphs(&mut self, mut f: Factory) {    
    let fonts = Search::ParentsThenKids(1, 1).for_folder("fonts").unwrap();
    let (sfont,_,_) = self.style.clone();
    let ref font = fonts.join(sfont);
    let mut g = Glyphs::new(font, f).unwrap();
    self.glyphs = Some(g);
  }
  fn place(&mut self,x: f64,y:f64) {
    self.position = (x,y)
  }
}



pub struct VisualElement {
  scene     : Scene<ImageType>,
  ts        : f64,
  time      : f64,
  text      : Vec<GameText>,
  recolor   : Option<[f32; 4]>,
  widgets   : Vec<Widget>
}
  

impl VisualElement {
  fn from(s : Scene<ImageType>, t: Vec<GameText>, c:  Option<[f32; 4]>,  w: Vec<Widget> ) -> VisualElement {
    VisualElement {
      scene   : s,
      ts      : 1.0,
      time    : 0.0,
      text    : t,
      recolor : c,  
      widgets : w
      
    }
  }
  fn new() -> VisualElement { VisualElement::from(Scene::new(), Vec::new(), None, Vec::new()) }
  fn display(&mut self,c: graphics::Context, g: &mut G2d, r: RenderArgs) {
    match self.recolor {
      Some(color) => {clear(color,g)}
      None        => {}
    }
    let tf = c.transform;
    self.scene.draw(tf,g);
    for t in self.text.iter_mut() { t.display(c,g,r) };
    for w in self.widgets.iter_mut() {w.display(c,g,r)};
  }
  fn update(&mut self, u: UpdateArgs) {
    self.time += u.dt;
    for w in self.widgets.iter_mut() {w.update(u)};
  }
}
      


  
pub struct GameRender {
  visual_elements: HashMap <u32,VisualElement> // I am forced to use u32 as the different visual scenes match to different Types... I use the GameDiv trait as glue.
}

impl GameRender {  
  pub fn new() -> GameRender {
    GameRender {
      visual_elements : HashMap::with_capacity(32)
    }
  }
  
      
  pub fn build_visual_elements(&mut self, r: RenderArgs,mut f: Factory) {
    self.visual_elements.insert( GameStage::Launch                          .convert()        ,  VisualElement::new());
    self.visual_elements.insert( GameStage::Loading                         .convert()        ,  build_loading(r,f.clone()));
    self.visual_elements.insert( GameStage::Menu(Page::Main(None))          .convert()        ,  build_main_menu(r,f.clone()));
    let a =  GameStage::Menu(Page::Main(Some(SubPage::NewGame))) .convert(); self.visual_elements.insert(a  ,  build_selector(a, f.clone()));
    let a =  GameStage::Menu(Page::Main(Some(SubPage::Load)))    .convert(); self.visual_elements.insert(a  ,  build_selector(a, f.clone()));
    let a =  GameStage::Menu(Page::Main(Some(SubPage::Options(None)))) .convert(); self.visual_elements.insert(a  ,  build_selector(a, f.clone()));
    let a =  GameStage::Menu(Page::Main(Some(SubPage::Quit)))    .convert(); self.visual_elements.insert(a  ,  build_selector(a, f.clone()));
    self.visual_elements.insert( GameStage::InGame(Phase::Cinematic(None))  .convert()        ,  VisualElement::new());
    self.visual_elements.insert( Page::Main(None)                           .convert()        ,  VisualElement::new());
    self.visual_elements.insert( Page::Quit                                 .convert()        ,  VisualElement::new());
    self.visual_elements.insert( SubPage::Options(None)                     .convert()        ,  VisualElement::new());
    self.visual_elements.insert( SubPage::Load                              .convert()        ,  VisualElement::new());
    self.visual_elements.insert( SubOption::Gameplay                        .convert()        ,  VisualElement::new());
    self.visual_elements.insert( SubOption::Graphics                        .convert()        ,  VisualElement::new());
    self.visual_elements.insert( SubOption::Sound                           .convert()        ,  VisualElement::new());
    self.visual_elements.insert( Phase::Battle(BattlePhase::Prepare)        .convert()        ,  VisualElement::new());
    self.visual_elements.insert( Phase::Cinematic(None)                     .convert()        ,  VisualElement::new());
    self.visual_elements.insert( Phase::Explore(None)                       .convert()        ,  VisualElement::new());
    self.visual_elements.insert( CMenu::Resume                              .convert()        ,  VisualElement::new());
    self.visual_elements.insert( CMenu::Skip                                .convert()        ,  VisualElement::new());
    self.visual_elements.insert( CMenu::Quit                                .convert()        ,  VisualElement::new());
    self.visual_elements.insert( GMenu::Resume                              .convert()        ,  VisualElement::new());
  }
  pub fn render(&mut self, c: graphics::Context, g: &mut G2d, r: RenderArgs, d: Vec<u32>) {
    for num in d.iter() {
      match self.visual_elements.get_mut(&num) {
        Some(sc) => { sc.display(c,g,r)}
        _        => {}
      }
    }
    
  }
  pub fn reset_time(&mut self,d: Vec<u32>,hard: bool) {
    match hard {
      true => { for sc in self.visual_elements.values_mut() { sc.time = 0.0;sc.ts   = 1.0;} }
      false=> { for num in d.iter() {
        match self.visual_elements.get_mut(&num) {
          Some(mut sc) => { sc.time = 0.0; sc. ts = 1.0;}
          _        => {}
        } }
      }
    }
  }
  pub fn update(&mut self,d: Vec<u32>, u:UpdateArgs) {
    for num in d.iter() {
        match self.visual_elements.get_mut(&num) {
          Some(mut sc) => { sc.update(u);}
          _        => {}
        }
    }
  }
}
    
pub fn build_loading(r: RenderArgs,  mut fact : Factory) -> VisualElement {
    let f = &mut fact.clone();
    let (wf,hf) = (r.width as f64, r.height as f64);
    print!("{} ,{}", wf.to_string(), hf.to_string());
    let uielems = Search::ParentsThenKids(1, 5).for_folder("loading").unwrap();
    let tex1 = Rc::new(Texture::from_path(
              f,
              uielems.join("maculed heart.png"),
              Flip::None,
              &TextureSettings::new()
          ).unwrap());
    let tex2 = Rc::new(Texture::from_path(
              f,
              uielems.join("larger heart.png"),
              Flip::None,
              &TextureSettings::new()
          ).unwrap());
    let mut sc = Scene::new();
    let mut hsprite = Sprite::from_texture(tex1.clone());
    let mut lhsprite = Sprite::from_texture(tex2.clone());

    hsprite.set_position(2.0*wf/3.0,hf/2.0);
    lhsprite.set_position(2.0*wf/3.0,hf/2.0);
    lhsprite.set_scale(1.2,1.2);
    lhsprite.set_opacity(0.6);
    let id1 = sc.add_child(hsprite);
    let id2 = sc.add_child(lhsprite);
    
    let mut loading_text = Vec::with_capacity(2);
    let mut l1 = GameText::from("Loading...".to_string());
    l1.style = ("DUBIEL.TTF".to_string(),22,BLACK);
    l1.place(0.0,0.9);
    l1.build_glyphs(fact);   
    loading_text.push(l1);
    
    let color = Some(WHITE);
    
    let texw = Rc::new(Texture::from_path(
              f,
              uielems.join("crown.png"),
              Flip::None,
              &TextureSettings::new()
          ).unwrap());
    let mut wsprite = Sprite::from_texture(texw.clone());
    wsprite.set_scale(0.6,0.6);
    let mut wsprite2 = Sprite::from_texture(texw.clone());
    wsprite2.set_scale(0.6,0.6);
    let tf1 = Transformation { transtype: TransType::Rotation, values: vec![0.0,0.0001,1.015] };
    let tf2 = Transformation { transtype: TransType::Tranself, values: vec![0.0,-2.0,1.0,1.00] };
    // let tfx = Transformation { transtype: TransType::Shear, values: vec![0.3,-0.3,1.001,1.001] };
    use std::f64::consts::PI;
    let tf3 = Transformation { transtype: TransType::Rotation, values: vec![PI,0.0,0.0] };
    let tf4 = Transformation { transtype: TransType::Rotation, values: vec![0.0,0.0001,1.015] };
    let tf5 = Transformation { transtype: TransType::Tranself, values: vec![0.0,-2.0,1.0,1.00] };
    // let tfy = Transformation { transtype: TransType::Shear, values: vec![0.3,-0.3,1.001,1.001] };
    let spritetfs = vec![tf1,tf2];
    let spritetfs2 = vec![tf4,tf3,tf5];
    let gsprite = GameSprite::from(wsprite,spritetfs);
    let gsprite2 = GameSprite::from(wsprite2,spritetfs2);
    let gsprites = vec![gsprite,gsprite2];
    let clock = Some(0.001);
    // let clock = None;
    let widget = Widget::from(gsprites, GameText::new(), None, (2.0/3.0,1.0/2.0), clock);
    let widgets = vec![widget];

    VisualElement::from(sc, loading_text, color,widgets)
}

pub fn build_main_menu(r: RenderArgs, mut fact:Factory) -> VisualElement {
  let main_menu = GameMenu::build_menu("Main");
  let mut menu_text = Vec::with_capacity(4);
  for s in ["NewGame","Load","Options","Quit"].iter() {
    let key = main_menu.structure.layout.get::<str>(s).unwrap();
    println!("here");
    let dpi =  main_menu.structure.display.get(key).unwrap(); 
    println!("there");
    let text =  (&dpi.text).to_string();
    let style = dpi.style.clone();
    let &(x1,x2,y1,y2) =  &dpi.location;
    let (px,py) = (x1 as f64 / x2 as f64, y1 as f64 / y2 as f64); 
    let mut mt = GameText::from(text);
    mt.style = style;
    mt.place(px,py);
    mt.build_glyphs(fact.clone());   
    menu_text.push(mt);
  }
  let f = &mut fact;
  let (wf,hf) = (r.width as f64, r.height as f64);
  let uielems = Search::ParentsThenKids(1 , 4).for_folder("menu").unwrap();
  let mut sc = Scene::new();
  for image in main_menu.images.iter() {
    let path = &image.name;
    let (x1,x2,y1,y2) = image.display;
    let (px,py) = (x1 as f64 / x2 as f64, y1 as f64 / y2 as f64); 
    let tex1 = Rc::new(Texture::from_path(
              f,
              uielems.join(path),
              Flip::None,
              &TextureSettings::new()
            ).unwrap());
    let mut isprite = Sprite::from_texture(tex1.clone());
    isprite.set_position(px*wf,py*hf);
    sc.add_child(isprite);
  }
  
  let color = Some(BLACK);
  VisualElement::from(sc, menu_text, color, Vec::new())
}

pub fn build_selector(c: u32, mut f: Factory) -> VisualElement {
  let uielems = Search::ParentsThenKids(1 , 4).for_folder("menu").unwrap();
  let texw = Rc::new(Texture::from_path(
              &mut f,
              uielems.join("selector.png"),
              Flip::None,
              &TextureSettings::new()
          ).unwrap());
  let mut wsprite = Sprite::from_texture(texw.clone());
  wsprite.set_scale(0.2,0.2); 
  let spritetfs = Vec::new();
  let gsprite = GameSprite::from(wsprite,spritetfs);
  let gsprites = vec![gsprite];
  let main_menu = GameMenu::build_menu("Main");
  let s = match c { // This is REALLY dirty. I should change this.
     6          => {"Options"}
     7          => {"Load"}
     26         => {"NewGame"}
     _          => {"Quit"}
  };
  let key = main_menu.structure.layout.get::<str>(s).unwrap();
  let dpi =  main_menu.structure.display.get(key).unwrap();
  let &(x1,x2,y1,y2) =  &dpi.location;
  let (px,py) = (x1 as f64 / x2 as f64, y1 as f64 / y2 as f64); 
  let widget = Widget::from(gsprites, GameText::new(), None, (px-0.1,py), Some(0.1));
  VisualElement::from(Scene::new(),Vec::new(), None, vec![widget])
}


pub struct Widget {
  elements  : Vec<GameSprite>,
  time      : f64,
  timescale : f64,
  clock     : Option<f64>,
  text      : GameText,
  recolor   : Option<[f32; 4]>,
  position  : (f64,f64)
}

impl Widget {
  pub fn new() -> Widget {
    Widget {
      elements  : Vec::new(),
      time      : 0.0,
      timescale : 1.0,
      clock     : None,
      text      : GameText::new(),
      recolor   : None,
      position  : (0.0,0.0)
    }
  }
  pub fn from(e: Vec<GameSprite>, text:GameText, rec: Option<[f32;4]>, position: (f64,f64), ck: Option<f64>) -> Widget {
    Widget {
      elements  : e,
      time      : 0.0,
      timescale : 1.0,
      text      : text,
      recolor   : rec,
      clock     : ck,
      position  : position
    }
  }
  pub fn display(&mut self,c: graphics::Context, g: &mut G2d, r: RenderArgs) {
    let (wf,hf) = (r.width as f64, r.height as f64);
    if let Some(clock) = self.clock {
      for gs in self.elements.iter() {
        let (x,y) = self.position;
        gs.tick_display(c.transform.trans(x*wf,y*hf),g,r);
      }
    } else {
      for gs in self.elements.iter() {
        let (x,y) = self.position;
        gs.smooth_display(c.transform.trans(x*wf,y*hf),g,r,self.time,self.timescale);
      }
    }
  }
  pub fn update(&mut self, u: UpdateArgs) {
    self.time += u.dt;
    if let Some(clock) = self.clock {
      if self.tick() {
        for e in self.elements.iter_mut() {e.timestep()};
        self.time = 0.0;
      }
    }
  }
  fn tick (&self) -> bool {

    self.time >= self.clock.unwrap()
  }
}

pub struct GameSprite {
  visual          : Sprite<ImageType>,
  transformations  : Vec<Transformation>
}

pub struct Transformation {
  transtype   : TransType,
  values       : Vec<f64>  // for rot : initial transformation, transformation per timestep, acceleration. 
}



#[derive(PartialEq,Eq,Clone,Copy)]
pub enum TransType { Rotation, Tranself, Translation, Transrelative, Scale, Flip, Shear}


impl GameSprite {
  pub fn smooth_display(&self,t:Matrix2d, g: &mut G2d, r: RenderArgs, time :f64, ts :f64){
    let (wf,hf) = (r.width as f64, r.height as f64);
    let mut transf = t;
    for tf in self.transformations.iter() {
      match tf.transtype {
        TransType::Rotation    => {
          let v = tf.values[0]*time*ts;
          transf = transf.rot_rad(v);
        }
        _                     => {}
        TransType::Tranself => {
            let [tx1,ty1,tx2,ty2] = self.visual.bounding_box();
            let (sw,sh) = (tx2-tx1,ty2-ty1); 
            let (vx, vy) = (tf.values[0],tf.values[1]);
            transf = transf.trans(vx*sw,vy*sh); 
          }
      
      }
      
    } 
    self.visual.draw(transf,g)
  }
  pub fn tick_display(&self,t:Matrix2d, g: &mut G2d, r: RenderArgs) {
    let (wf,hf) = (r.width as f64, r.height as f64);
    let mut transf = t;
    for tf in self.transformations.iter() {
      match tf.transtype {
        TransType::Rotation    => {
          let v = tf.values[0];
          transf = transf.rot_rad(v);
        }
        
        TransType::Tranself => {
          let [tx1,ty1,tx2,ty2] = self.visual.bounding_box();
          let (sw,sh) = (tx2-tx1,ty2-ty1); 
          let (vx, vy) = (tf.values[0],tf.values[1]);
          transf = transf.trans(vx*sw,vy*sh); 
        }
        TransType::Translation => {
          let (vx, vy) = (tf.values[0],tf.values[1]);
          transf = transf.trans(vx,vy); 
        }
        TransType::Transrelative => {
          let (vx, vy) = (tf.values[0],tf.values[1]);
          transf = transf.trans(vx*wf,vy*hf); 
        }
        TransType::Scale       => {
          let (vx, vy) = (tf.values[0],tf.values[1]);
          transf = transf.scale(vx,vy);
        }
        TransType::Flip        => {
          match (tf.values[0] as u32,tf.values[1] as u32) {
            (1,0) => { transf = transf.flip_h() }
            (1,1) => { transf = transf.flip_hv() }
            (0,1) => { transf = transf.flip_v() }
            _     => {}
          }
        }
        TransType::Shear       => {
          transf = transf.shear([tf.values[0], tf.values[1]]);
        }
        _   => {}
      }
    }
      self.visual.draw(transf,g)
  }
  pub fn from(s: Sprite<ImageType>, tfs: Vec<Transformation>) -> GameSprite{
    GameSprite {
      visual : s,
      transformations : tfs
    }
  }
  pub fn timestep(&mut self) {
    for tf in self.transformations.iter_mut() {
      match tf.transtype {
        TransType::Rotation     => {
          tf.values[0] += tf.values[1];
          tf.values[1] *= tf.values[2];
        }
        TransType::Tranself | TransType::Translation | TransType::Transrelative | TransType::Scale | TransType::Shear    => {
          tf.values[0] *= tf.values[2];
          tf.values[1] *= tf.values[3];
        }
        _                       => {}
      }
    }
  }
}