use ::json;
use ::find_folder::*;

use ::app::game::convenience;

use std::collections::HashMap;


use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::io::BufReader;
use std::error::Error;


pub struct MenuItemDisplayInformation {
  pub location  : (u32,u32,u32,u32),
  pub style     : Style,
  pub image     : Option<String>,
  pub text      : String
}

pub struct MenuStructure {
    //cycle of hashmaps
    pub layout    : HashMap<String,(u32,u32)>,
    pub display   : HashMap<(u32,u32),MenuItemDisplayInformation>,
    pub select    : HashMap<(u32,u32,u32,u32), String> 
}

impl MenuStructure {
  pub fn new() -> MenuStructure {
    MenuStructure {
      layout    : HashMap::new(),
      display   : HashMap::new(),
      select    : HashMap::new()
    }
  }
}

pub struct GameMenu {
    pub selector  : (u32,u32),
    pub structure : MenuStructure,
    pub title     : String,
    pub images    : Vec<MenuImage> 
}


impl GameMenu {
  pub fn new() -> GameMenu {
    GameMenu {
      selector  : (0,0),
      structure : MenuStructure::new(),
      title     : "".to_string(),
      images    : Vec::new()
    }
  }
  pub fn build_menu(s: &str ) -> GameMenu {
    let title = s.to_string();
    let (menu_elements,menu_images) = get_menu_info(s.to_string());
    let menu_structure = build_menu_structure(menu_elements);
    let gmenux = GameMenu {
      selector  : (0,0),
      structure : menu_structure,
      title     : title,
      images    : menu_images
    };
    return gmenux;
    fn build_menu_structure(v: Vec<MenuElement>) -> MenuStructure {
      let mut layout = HashMap::new();
      let mut display = HashMap::new();
      let mut select = HashMap::new();
      for elem in v.iter() {
        let midi = MenuItemDisplayInformation {
          location  : elem.display,
          style     : elem.style.clone(),
          image     : elem.image.clone(),
          text      : elem.text.clone()
        };
        layout.insert(elem.label.clone(), elem.position);
        display.insert(elem.position, midi);
        select.insert(elem.display, elem.label.clone());
      }
      MenuStructure {
        layout: layout,
        display: display,
        select: select
      }
    }

  }
  
  
}

pub type Style = (String, u32,[f32; 4]);

pub struct MenuImage {
  pub name    : String,
  pub display : (u32,u32,u32,u32) 
}


pub struct MenuElement{
  pub label : String,
  pub text: String,
  pub position: (u32,u32),
  pub display:(u32,u32,u32,u32),
  pub style: Style,
  pub image: Option<String>
}

fn get_menu_info(s:String) -> (Vec<MenuElement>,Vec<MenuImage>) {
  let path = Search::ParentsThenKids(3, 3).for_folder("menus").unwrap();
  let file = match File::open(path.join(s+".json")) {
    Err(why) => { panic!("couldn't open the menu file : {}", why.description())}
    Ok(file) => {println!("json loaded"); file}
  };
  let mut buf_reader = BufReader::new(file);
  let mut contents = String::new();
  buf_reader.read_to_string(&mut contents).unwrap();
  let parsed = json::parse(&contents).unwrap();
  let raw_menu_elements = &parsed["elements"];
  let raw_menu_images   = &parsed["images"];
  let mut menu_elems = Vec::new();
  for raw_menu_element in raw_menu_elements.members() {
    let label = raw_menu_element["label"].as_str().unwrap();
    let text = raw_menu_element["text"].as_str().unwrap();
    print!("{}, {}", label, text);
    let (x,y) = (raw_menu_element["position"][0].as_u32().unwrap(),raw_menu_element["position"][1].as_u32().unwrap());
    let (x1,x2,y1,y2) = (raw_menu_element["display"][0].as_u32().unwrap(),raw_menu_element["display"][1].as_u32().unwrap(),raw_menu_element["display"][2].as_u32().unwrap(),raw_menu_element["display"][3].as_u32().unwrap());
    let raw_font = raw_menu_element["style"][0].as_str().unwrap();
    let raw_size = raw_menu_element["style"][1].as_u32().unwrap();
    let raw_color = raw_menu_element["style"][2].as_str().unwrap().to_string();
    let raw_image = raw_menu_element["image"].as_str().unwrap();
    let style = (raw_font.to_string(), raw_size, convenience::match_color(raw_color.to_string()));
    let image = match raw_image {
      "None"  => None,
      _       => None
    };
    let menu_elem = MenuElement {
      label: label.to_string(),
      text: text.to_string(),
      position: (x,y),
      display: (x1,x2,y1,y2),
      style:  style,
      image: image
    };
    menu_elems.push(menu_elem);
  }
  let mut menu_images = Vec::new();
  for raw_menu_image in raw_menu_images.members() {
    let name = raw_menu_image["name"].as_str().unwrap();
    let (x1,x2,y1,y2) = (raw_menu_image["display"][0].as_u32().unwrap(),raw_menu_image["display"][1].as_u32().unwrap(),raw_menu_image["display"][2].as_u32().unwrap(),raw_menu_image["display"][3].as_u32().unwrap());
    let menu_image = MenuImage {
      name    : name.to_string(),
      display : (x1,x2,y1,y2)
    };
    menu_images.push(menu_image);
  }
  let menu_info = (menu_elems, menu_images);
  menu_info
}