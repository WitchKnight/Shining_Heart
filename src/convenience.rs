
pub const WHITE:  [f32; 4] = [1.0, 1.0, 1.0, 1.0];
pub const GREEN:  [f32; 4] = [0.0, 1.0, 0.0, 1.0];
pub const RED:    [f32; 4] = [1.0, 0.0, 0.0, 1.0];
pub const BLUE:   [f32; 4] = [0.0, 0.0, 1.0, 1.0];
pub const BLACK:  [f32; 4] = [0.0, 0.0, 0.0, 1.0];
pub const TRSP:   [f32; 4] = [0.0, 0.0, 0.0, 0.0];

pub fn match_color(s: String) -> [f32; 4] {
    match s.as_str() {
        "WHITE" => WHITE,
        "GREEN" => GREEN,
        "RED"   => RED,
        "BLUE"  => BLUE,
        "BLACK" => BLACK,
        _       => TRSP
    }
}











macro_rules! hmap(
    { $($key:expr => $value:expr),+ } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key, $value);
            )+
            m
        }
     };
);

macro_rules! btreemap {
    // trailing comma case
    ($($key:expr => $value:expr,)+) => (btreemap!($($key => $value),+));
    
    ( $($key:expr => $value:expr),* ) => {
        {
            let mut _map = ::std::collections::BTreeMap::new();
            $(
                _map.insert($key, $value);
            )*
            _map
        }
    };
}

