


use ::gfx_device_gl;
use ::gfx_device_gl::Factory;
use ::ai_behavior::{
    Action,
    Sequence,
    Wait,
    WaitForever,
    While,
};

use ::piston_window::*;
use ::piston_window;
use ::sprite::*;


use std::collections::*;
use std::rc::Rc;
use std::path::*;
use std::fmt::{self, Debug, Display};

#[path = "convenience.rs"]
#[macro_use]
mod convenience;

pub type ImageType = piston_window::Texture<gfx_device_gl::Resources>;

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum GameStage { 
  Menu(Page), 
  InGame(Phase), 
  Loading, 
  Launch
}
#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum Page {
  Main(Option<SubPage>),
  Quit
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum SubPage {
  Options(Option<SubOption>), 
  Load,    
  Save
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum SubOption { Sound, Gameplay, Graphics }

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum Phase { 
  Battle(BattlePhase),
  Cinematic(Option<CMenu>),
  Explore(Option<GMenu>)
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum CMenu {
  Resume,
  Skip,
  Quit
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum GMenu {
 Resume
}



#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
pub enum BattlePhase { Prepare, Turn, Combat, Event, Check, End }

 

 
 
 




impl fmt::Display for GameStage { // this is just here to allow me to call  .to_string()
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
    Debug::fmt(self, f)
  }
}

impl GameStage {
  pub fn get_scene(&self, r: RenderArgs,mut f: Factory) -> Scene<ImageType> {
    match self {
      &GameStage::Launch    => {Scene::new()}
      &GameStage::Loading   => {Self::display_loading(r,f)}
      &GameStage::Menu(_)      => {Scene::new()}
      &GameStage::InGame(_)    => {Scene::new()}
    }
  }
  fn display_loading(r: RenderArgs,mut f:Factory) -> Scene<ImageType> {
    let (wf,hf) = (r.width as f64, r.height as f64);
    print!("{} ,{}", wf.to_string(), hf.to_string());
    let uielems = PathBuf::from("../assets/ui/loading");
    let tex1 = Rc::new(Texture::from_path(
              &mut f,
              uielems.join("maculed heart.png"),
              Flip::None,
              &TextureSettings::new()
          ).unwrap());
    let tex2 = Rc::new(Texture::from_path(
              &mut f,
              uielems.join("larger heart.png"),
              Flip::None,
              &TextureSettings::new()
          ).unwrap());
    let mut sc = Scene::new();
    let mut hsprite = Sprite::from_texture(tex1.clone());
    let mut lhsprite = Sprite::from_texture(tex2.clone());
    
    hsprite.set_position(2.0*wf/3.0,hf/2.0);
    lhsprite.set_position(2.0*wf/3.0,hf/2.0);
    lhsprite.set_scale(1.2,1.2);
    lhsprite.set_opacity(0.6);
    let id1 = sc.add_child(hsprite);
    let id2 = sc.add_child(lhsprite);

    sc
    
  }
}
